import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import axios from 'axios'
import Menu from './Component/Menu';
import HomePage from './Component/HomePage';
import NotFound from './Component/NotFound';
import Add from './Component/Add';
import View from './Component/View';
import Update from './Component/Update';

export default class App extends Component {
  render() {
    return (
      <div>
        <Router>
          <Menu />
          <h1></h1>
          <Switch>
            <Route path='/' exact  component={HomePage} />
            <Route path='/add'  component={Add} />
            <Route path='/view/:id'  component={View}/>
            <Route path='/edit/:id'  component={Update}/>
            <Route path='*' component={NotFound} />
          </Switch>
        </Router>
      </div>
    )
  }
}

