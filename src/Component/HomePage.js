import {Button,Table} from 'react-bootstrap'
import {Link} from 'react-router-dom' 
import axios from 'axios'

import React, { Component } from 'react'

export default class HomePage extends Component {
    constructor(props){
        super(props);
        this.state = {
            users: [],
        };
    }

    componentDidMount(){
        axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15").then((res) => {
            console.log(res.data.DATA);
            this.setState({
                users : res.data.DATA,
            });
        });
    }
    
    handleDelete=(e)=>{
        axios.delete(`http://110.74.194.124:15011/v1/api/articles/${e.target.value}`)
            .then((res) => {
              alert(res.data.MESSAGE);
              this.componentWillMount()
            })
            .catch((err) => {
                console.log(err);
              });
  
     }

     convertDate=(dateStr)=>{
        let ds = dateStr;
        let year = ds.substring(0, 4);
        let month = ds.substring(4, 6);
        let day = ds.substring(6, 8);
        let date = year + "-" + month + "-" + day;
        return date;
    }

    render() {
        return (
            <div className='container'>
            <center>
                <h1>Artical Management</h1>
                <Button as={Link} to='/add' variant="dark" > Add New Artical </Button>
            
            <h1></h1>
            <Table striped bordered hover>
            <thead>
                <tr>  
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>CREATE DATE</th>
                    <th>IMAGE</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {this.state.users.map((users)=> 
                    <tr key={users.ID}>
                    <td>{users.ID}</td>
                    <td>{users.TITLE}</td>
                    <td>{users.DESCRIPTION}</td>
                    <td>{this.convertDate(users.CREATED_DATE)}</td>
                    <td><img src={users.IMAGE} /></td>
                    <td>
                        <Link to={`/view/${users.ID}`}><Button variant='primary' >View</Button></Link>&nbsp;  
                        <Link to={`/edit/${users.ID}`}><Button variant="warning" >Edit</Button></Link>&nbsp;
                        <Button variant="danger" onClick={this.handleDelete.bind(this)} value={users.ID}>Delete</Button>
                    </td>
                </tr>)}
            </tbody>
            </Table>
            </center>
        </div>
        )
    }
}





