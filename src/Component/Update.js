import React, { Component } from 'react'
import axios from 'axios'
import {Media,Form,Button} from 'react-bootstrap'
export default class Update extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          title: "",
          description: "",
          image: "",
          data: {},
        };
    
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      componentDidMount() {
        let id = this.props.match.params.id;
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
          .then((res) => {
            this.setState({
              data: res.data.DATA,
              title: res.data.DATA.TITLE,
              image: res.data.DATA.IMAGE,
              description: res.data.DATA.DESCRIPTION,
            });
            console.log(res.data.DATA.TITLE);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    
      handleChange = (event) => {
        this.setState({
          [event.target.name]: event.target.value,
        });
      };
    
      handleSubmit(e) {
        let id = this.props.match.params.id;
        let title = this.state.title;
        let description = this.state.description;
        let image = this.state.image;
        console.log(this.emptyTitle);
    
        if (title === "" && description === "") {
          alert("* Both Field Cannot Be Blank");
        } else {
          let text = {
            TITLE: title,
            DESCRIPTION: description,
            IMAGE: image,
          };
          axios.put(`http://110.74.194.124:15011/v1/api/articles/${id}`, text).then(
            (res) => {
              alert(res.data.MESSAGE);
            //   this.props.history.push("/");
            }
          );
        }
    
        this.setState({
          title: "",
          description: "",
        });
        e.preventDefault();
      }
    
    render() {
        let d = this.state.data;
        return (
            <div className='container'>           
               <Form onSubmit={this.handleSubmit}>
                   <h1>Update Artical</h1>
               <Media>
                    <Media.Body>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" name="title" placeholder="Enter Title" value={this.state.title} onChange={this.handleChange}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" name="description" placeholder="Enter Description" value={this.state.description} onChange={this.handleChange} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Save
                            </Button>
                    </Media.Body>
                    <img
                        width={300}
                        height={300}
                        className="ml-3"
                        src={this.state.image}
                        alt="Generic placeholder"
                />
                </Media>
                
              </Form>
            </div>
        )
    }
}
