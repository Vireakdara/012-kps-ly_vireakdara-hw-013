import React, { Component } from 'react'
import {Media,Form,Button} from 'react-bootstrap'
import axios from 'axios'

export default class Add extends Component {
    constructor(props){
       super(props);
       this.state = {
            title: "",
            description: ""
       };

       this.handleChange = this.handleChange.bind(this);
       this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleChange = (event) => {
        this.setState({
          [event.target.name]: event.target.value,
        });
      };
      handleSubmit(e) {
        e.preventDefault();
        let title = this.state.title;
        let description = this.state.description;
    
        if (title === "" && description === "") {
          alert("* Both Field Cannot Be Blank");
        } else {
          let text = {
            TITLE: this.state.title,
            DESCRIPTION: this.state.description,
            IMAGE:"https://www.logaster.com/blog/wp-content/uploads/2018/05/LogoMakr.png",
          };
          axios.post("http://110.74.194.124:15011/v1/api/articles", text).then(
            (res) => {
              alert(res.data.MESSAGE);
              this.props.history.push("/"); 
            }
          );
        }
    
        this.setState({
          title: "",
          description: "",
        });
      }
  

    render(){
        return (
            <div className='container'>           
               <form  onSubmit= {this.handleSubmit}>
                   <h1>Add Artical</h1>
               <Media>
                    <Media.Body>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control type="text" name="title" placeholder="Enter Title" onChange={this.handleChange}/>
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" name="description" placeholder="Enter Description" onChange={this.handleChange}/>
                                
                            </Form.Group>
                            <Button variant="primary" type="submit" >
                                Submit
                            </Button>
                    </Media.Body>
                    <img
                        width={300}
                        height={300}
                        className="ml-3"
                        src="https://www.logaster.com/blog/wp-content/uploads/2018/05/LogoMakr.png"
                        alt="Generic placeholder"
                    />
                </Media>
              </form>
            </div>
      )
    }
}
