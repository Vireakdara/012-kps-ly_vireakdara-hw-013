import {Media} from 'react-bootstrap'
import React, { Component } from 'react'
import axios from 'axios'
let id;
export default class View extends Component {
    constructor() {
        super();
        this.state = {
          id: "",
          dataView: {},
        };
      }

      componentDidMount() {
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
          .then((res) => {
            this.setState({
                dataView: res.data.DATA,
            });
          })
      }

  render() {     
    var data = this.props.match.params.id;
    id=data;
    return (
        <div className="container">
            <h1>{this.state.dataView.TITLE}</h1>
                <Media>
                    <img
                        width={400}
                        height={300}
                        className="mr-3"
                        src={this.state.dataView.IMAGE}
                        alt="Generic placeholder"
                    />
                    <Media.Body>
                        <p>{this.state.dataView.DESCRIPTION}</p>
                    </Media.Body>
                    </Media>
        </div>
    );
    }}





